﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interfaces
{
    public interface IImage
    {
        int ImageID { get; set; }
        float Evaluate { get; set; }
        int Label { get; set; }
        string Feature { get; set; }
    }
}
