﻿using Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAOMock1.BO
{
    public class Image : IImage
    {
        public int ImageID 
        { 
            get;
            set;
        }

        public float Evaluate
        {
            get;
            set;
        }

        public int Label
        { 
            get;
            set;
        }

        public string Feature
        {
            get;
            set;
        }
    }
}
