﻿using Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAOMock1
{
    public class DAO : IDAO
    {
        private List<IImage> _cars;

        public DAO()
        {

            _cars = new List<IImage>()
            {
                new BO.Image() {ImageID=0, Label=1, Feature="Black", Evaluate=1},
                new BO.Image() {ImageID=1, Label=2, Feature="Black", Evaluate=2},
            };
        }


        public IEnumerable<IImage> GetAllCars()
        {
            return _cars;
        }


        public IImage CreateNewCar()
        {
            return new BO.Image();
        }

        public void AddCar(IImage image)
        {
            _cars.Add(image);
        }
    }
}
