﻿using Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mvvmtest
{
    public class CarViewModel : INotifyPropertyChanged, INotifyDataErrorInfo
    {
        private IImage _image;

        public CarViewModel(IImage image)
        {
            _image = image;
            Validate();
        }

        public int ImageID
        {
            get { return _image.ImageID; }
            set // sprawdzenie jakieś
            {
                _image.ImageID = value;
                RaisePropertyChanged("ImageID");
            }
        }

        public float Evaluate
        {
            get { return _image.Evaluate; }
            set
            {
                _image.Evaluate = value;
                RaisePropertyChanged("Evaluate");
            }
        }

        public int Label
        {
            get { return _image.Label; }
            set
            {
                _image.Label = value;
                RaisePropertyChanged("Label");
            }
        }

        public string Feature
        {
            get { return _image.Feature; }
            set
            {
                _image.Feature = value;
                RaisePropertyChanged("Feature");
            }
        }

        private void RaisePropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
                Validate();
            }
        }

        // INotifyDataErrorInfo
        public event PropertyChangedEventHandler PropertyChanged;


        private Dictionary<string, List<string>> _validationErrors = 
            new Dictionary<string, List<string>>(); // do przechowywania listy komunikatów błedów

        public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;

        public System.Collections.IEnumerable GetErrors(string propertyName)
        {
            if (string.IsNullOrEmpty(propertyName) || !_validationErrors.ContainsKey(propertyName))
                return null;

            return _validationErrors[propertyName]; // lista bledow dla danej wlasciwosci
        }

        public bool HasErrors
        {
            get {
                return _validationErrors.SelectMany(x => x.Value).ToList().Count > 0; // splaszczamy liste, jezeli cos w niej jest to znaczy, ze instnieje błąd
            }
        }
        // koniec INotifyDataErrorInfo

        public void Validate()
        {
            List<string> errors = new List<string>();

//            if (this.Name == null)
//                errors.Add("Name cannot be null.");
//            else
//            {
//                if (this.Name.Length < 3)
//                {
//                    errors.Add("Name must be longer than 3 characters.");
//                }
//            }
//
//            _validationErrors["Name"] = errors;

            errors = new List<string>();

            if (this.Evaluate < 10)
            {
                errors.Add("Evaluate cannot be lower than 10");
            }

            if (this.Evaluate < 5)
            {
                errors.Add("Evaluate cannot be lower than 5"); // przykład dydaktyczny
            }

            _validationErrors["Evaluate"] = errors;
        }
    }
}
